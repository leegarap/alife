#pragma once
#include< string >
#include<algorithm>
/*
	resource Object including ground,water and plant
*/
class resourceObject
{
public:
	resourceObject(std::string name,int x,int y,int  water,int food,int growing_rate);
	~resourceObject();
	std::string getResourceName();
	int getResourceWater();
	int getResourceFood();
	void growing();
	void increaseResource(int water_val,int food_val);
	std::pair<int, int> getPos();
private:
	int water_resource, food_resource,growing_rate,cur_food,cur_water;
	std::string name;
	int x, y;
};

