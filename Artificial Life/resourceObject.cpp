#include "resourceObject.h"


resourceObject::resourceObject(std::string name, int x, int y,int water,int food, int growing_rate)
{
	food_resource = food;
	water_resource = water;
	cur_food = food_resource;
	cur_water = water_resource;
	resourceObject::growing_rate = growing_rate;
	resourceObject::name = name;
	resourceObject::x = x;
	resourceObject::y = y;
}
std::string resourceObject::getResourceName() {
	return name;
}
int resourceObject::getResourceWater() {
	return cur_water;
}
int resourceObject::getResourceFood() {
	return cur_food;
}

void resourceObject::increaseResource(int water_val, int food_val) {
	cur_water = std::max(std::min(water_resource, cur_water + water_val),0);
	cur_food = std::max(std::min(food_resource, cur_food + food_val), 0);
}
void resourceObject::growing() {
	increaseResource(growing_rate, growing_rate);
}

resourceObject::~resourceObject()
{
}
std::pair<int, int> resourceObject::getPos() {
	return std::make_pair(x,y);
}