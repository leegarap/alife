#pragma once
#include<iostream>
#include "Matrix.h"
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<vector>
class ANN
{
public:
	ANN(int input,int hidden,int output);//Construct Simple Neurak Network (3layers) 
	~ANN();
	Matrix feedforward(Matrix &INPUT);//Feed forward (INPUT is 1 X inputs shape matrix)
	void mutation(double mutation_rate);//Mutation for NeuroEvolution
	ANN CrossingOverTo(ANN &A, int new_input, int new_hidden);//Same as mutation
	Matrix getHidden();//return Matrix of weigt from INPUT layer to Hidden layer
	Matrix getOutput();//As same as above but Output layer

	//Warning Critical bug
	void setCustomHidden(Matrix *newHidden);//Set Hidden Layer
	void setCustomOutput(Matrix *newOutput);//Set Output Layer
private:
	int inputs, hiddens, outputs;//shape of Neural Network
	Matrix *HIDDEN_W,*OUTPUT_W;//Weight of Neural Network

};

