#pragma once
#include "Organism.h"
#include "Omnivore.h"
#include "Carnivore.h"
#include "Herbivore.h"
#include<vector>
#include<cstdlib>
#include<ctime>
#include<iostream>
#include<algorithm>
#include<string>
#include "resourceObject.h"
#define WATER 100
#define FOOD 100
#define GR 10
class Terrain
{
public:
	Terrain();
	void initTerrain(int N, int M);
	void initTerrainBySave(int N, int M,int map[50][50]);
	~Terrain();
	void showTable();
	std::vector<std::shared_ptr<Organism> > findOrganism(Organism &O);
	std::vector<resourceObject * > findWater(Organism &O);
	std::vector<resourceObject * > findPlant(Organism &O);
	std::vector< std::pair<int, int> > findEmptySpace(Organism &O);
	std::vector<double> getEnv(Organism &O);
	bool changePosition(int from_x, int from_y, int to_x, int to_y);
	bool setOrganism(std::shared_ptr< Organism > *org, int x, int y);
	bool canOccupy(int x, int y);
	
	std::vector <int>& operator[] (int x) {
		return table[x];
	}




private:
	int M, N;//size of terrain
	std::vector< std::vector <int> > table;
	std::vector< std::vector <  resourceObject*> > res;
	std::vector< std::vector <int> > occp;
	std::vector< std::vector <std::shared_ptr<Organism> > > org;
	
	//resourceObject resource[3] = { resourceObject("G",0,0),resourceObject("P",0,10),resourceObject("W",10,0) };
	

};

