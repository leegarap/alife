#include "Organism.h"



Organism::Organism(char type, int fHP, int dHP, int size, int sMin, int sMax, int age, int reproductive_rate, int posX, int posY, int radius,ANN *brain)
{
	Organism::type = type;
	Organism::fHP = fHP;
	Organism::dHP = dHP;
	Organism::size = size;
	Organism::sMin = sMin;
	Organism::sMax = sMax;
	Organism::age = age;
	
	Organism::reproductive_rate = reproductive_rate;
	Organism::posX = posX;
	Organism::posY = posY;
	Organism::radius = radius;
	isAlive = true;

	curSpeed = (rand() % sMax)+1;
	Organism::brain = brain;
	
}
/*
	Function move(direction:integer) :
		move positon of the organism in 4 directions
			0:Nove up
			1:Move left
			2:Move down
			3:move right
*/
void Organism::move(int direction, Terrain &t) {
	int tposY = posY, tposX = posX;
	switch (direction) {
	case 0:
		tposY--;
		break;
	case 1:
		tposX++;
		break;
	case 2:
		tposY++;
		break;
	case 3:
		tposX--;
		break;
	}
	if (t.changePosition(posX, posY,tposX, tposY)) {
		//std::cout << "move!!" << std::endl;
		posX = tposX;
		posY = tposY;
	}
	else {

	}
}

void Organism::reproduction(Terrain &t) {
	std::vector<std::shared_ptr<Organism> > org = t.findOrganism(*this);
	std::vector< std::pair<int, int> > emp = t.findEmptySpace(*this);
	if (emp.empty())return;
	if (org.empty())return;
	int index = rand() % org.size();
	double m_rate = 0.3;//Mutation_rate=0.3;
	double rat = (rand() % 100) / 100.0;
	std::pair<int, int> newPos = emp[rand() % emp.size()];
	int n_type = ((double)(rand()%10)*1.0/10.0)>m_rate?(rand()%2==0?this->getType():org[index]->getType()):(rand()%3==0?'C':(rand()%2==0?'H':'O'));
	rat = (rand() % 100) / 100.0;
	int n_fHP = (rat *(double)this->getfHP()+ (1.0- rat)*org[index]->getfHP());
	rat = (rand() % 100) / 100.0;
	int n_dHP = (rat *(double)this->getdHP() + (1.0 - rat)*org[index]->getdHP()) ;

	int n_size = (rat *(double)this->getSize() + (1.0 - rat)*(double)org[index]->getSize()) ;
	if ((double)(rand() % 10) / 10 <= m_rate) {
		 if (n_size + ((rand() % (int)(n_size / 3))*(rand() % 2 == 0 ? 1 : -1)) > 1) {
			 n_size = n_size + ((rand() % (int)(n_size / 3))*(rand() % 2 == 0 ? 1 : -1));
		 }
		 else {
			 n_size = 1;
		 }
	}
	int n_sMin = sMin;
	int n_sMax = sMax;
	int n_age = age;

	int n_reproductive_rate = reproductive_rate;
	int n_posX = newPos.first;
	int n_posY = newPos.second;
	int n_radius = (rat *(double)this->getRadius() + (double)(1 - rat)*(double)org[index]->getRadius()) ;
	if ((double)(rand() % 10) / 10 <= m_rate)n_radius += (rand() % (n_radius / 3))*(rand() % 2 == 0 ? 1 : -1);

	auto n_brain = brain;
}

/*
	Function think:
		gather status of Organism + outside of state of organism(relating to radius status)

*/
int Organism::think(Terrain &T) {
	return rand() % 6;
}


Organism::~Organism()
{
	delete(brain);
}
//---------------------------------------------------------------------------------------
int Organism::getRadius() const {
	return radius;

}
int Organism::getSpeed() const {
	return curSpeed;
}
int Organism::getSize() const {
	return size;
}
int Organism::getfHP() const {
	return fHP;

}
int Organism::getdHP()const {
	return dHP;
}
int Organism::getMaxfHP()const {
	return 5 * size;
}
int Organism::getMaxdHP() const {
	return 6 * size;
}
std::pair<int, int> Organism::getPos() {
	return std::make_pair(posX, posY);

}
char Organism::getType() const {
	return Organism::type;
}
//-------------------------------------------------------------------
void Organism::increaseSize(int c) {
	size += c;
}
void Organism::increaseSpeed(int c) {
	curSpeed += c;
}
void Organism::increasefHP(int c)  {
	fHP += c;
}
void Organism::increasedHP(int c) {
	Organism::dHP += c;
}
bool Organism::is_Dead()  {
	if (!isAlive)return true;
	if (fHP <=0 || dHP <=0) { 
		type = 'D';
		isAlive = false;
		return true;
	}
	return false;
}


/*
 this function get all organisms around itself and determine to eat it with prob
*/
void Organism::eatOrganism(Terrain &t) {
	std::vector<std::shared_ptr< Organism > > Org_in_range=t.findOrganism(*this);//get all surrounding organisms
	//std::vector<resourceObject *> water_in_range = t.findWater(*this);
	//std::vector<resourceObject *> plant_in_range = t.findPlant(*this);
	
	if (type == 'C' || type=='O') { // check type of the organism
		float prob=0.0, MaxProb=0.0;
		std::shared_ptr< Organism > goodProb=nullptr;
		for(auto &org:Org_in_range) {
			if (org->is_Dead())continue;
			//Probably to eat a organism =8*(size of this organism/size of that organism)+0.2*(that organism HP)-0.3*that organism speed
			prob = ((float)size / (float)org->getSize()) * 8 + (float)(org->getfHP()*1.0)*0.2 - (float)(org->getSpeed()*1.0)*0.3;
			
			if (prob > MaxProb) {//find max prob
				MaxProb = prob;
				goodProb = org;
			}
		}
		//std::cout << MaxProb << std::endl;
		if (goodProb == NULL || (double)(rand()%100)>MaxProb) {
			return ;
		}
		else {//If eating the organism increase itself fHP
			//std::cout <<"("<<posX<<","<<posY<<")"<<" Eats " << "(" << goodProb->getPos().first<< "," << goodProb->getPos().second << ")!!!"<<std::endl;
			fHP=std::min( this->getMaxfHP(), fHP +goodProb->getfHP() / 2);
			goodProb->increasefHP(-(goodProb->getfHP())-99);//Set Eaten Organism fHP to -99
			goodProb->is_Dead();
		}
	}
	else {
		return;

	}
	return;
}
void Organism::eatPlant(Terrain &t) {
	std::vector<resourceObject *> plant_in_range = t.findPlant(*this);
	
	if (type == 'H' || type == 'O') {
		if (!plant_in_range.empty()) {
			//std::cout << " Eating... \n";
			int ran_plant = rand() % plant_in_range.size();
			int amount = std::min(this->getMaxfHP()-fHP, plant_in_range[ran_plant]->getResourceFood());
			if (type == 'H') {

				fHP = std::min(fHP+ amount,this->getMaxfHP());
				plant_in_range[ran_plant]->increaseResource(0,-amount);
			}
			else {
				fHP = std::min(fHP + amount /2, this->getMaxfHP());
				plant_in_range[ran_plant]->increaseResource(0, -amount/2);
			}
		}
	}
	else {
		return;

	}
	return;
}
void Organism::eat(Terrain &t) {
	eatPlant(t);
	eatOrganism(t);
}

void Organism::drink(Terrain &t) {
	std::vector<resourceObject *> water_in_range = t.findWater(*this);
	
		if (!water_in_range.empty()) {
			//std::cout << "Drinking...\n";
			int ran_water = rand() % water_in_range.size();
			int amount = std::min(this->getMaxdHP() - dHP, water_in_range[ran_water]->getResourceWater());
			dHP =std::min(dHP+ amount,this->getMaxdHP());
			water_in_range[ran_water]->increaseResource(-amount,0);

		}
}

void Organism::action(Terrain &t) {
	if (!isAlive)return;
	int curTime = curSpeed;
	int option;
	increasefHP(-1);
	increasedHP(-1);
	is_Dead();
	while (curTime--) {
		//std::cout << curTime << "/" << curSpeed << ".\n";
		option = this->think(t);
		//std::cout << "!" << option << "!" << std::endl;
		if (option < 4) {

			move(option,t);
		}
		else {

			switch (option) {
			case 4:
				eat(t);
				break;
			case 5:
				drink(t);
				break;
			}
		}
	}
	//std::cout<<std::endl;


}

void Organism::show() {

	std::cout << "Status:";
	if (isAlive) {
		std::cout << "Alive ";
	}
	else {
		std::cout << "Died  ";
	}
	std::cout<<"Type:"<<type << " Food:" << fHP << "/" << getMaxfHP() << " Water:" << dHP << "/" << getMaxdHP() << " Size:" << size <<" X:"<<posX<<" Y:"<<posY<<std::endl;
}