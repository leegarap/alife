﻿#pragma once
#include<iostream>
#include<vector>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<string>

class Matrix
{
public:
	Matrix(int N, int M);
	Matrix(std::vector<double> &raw, int N, int M);//Constructing matrix from 1D linked list
	~Matrix();
	void reshape(int N, int M);//Simple reshape Matrix
	int *shape();//Get shape of the Matrix.2D array-> N,M
	void show();//Print Matrix
	Matrix CrossTo(Matrix &A);//Matrix Multiplication
	std::vector < std::vector < double >> getMatrixAsVector();//get vector(easy to handle)
	void relu();//Activation Function,nake Neural Network Nonelinear,I get from Deep learning
private:
	std::vector< std::vector<double> > mat;//Store All data
	int shape_N, shape_M;//Shape of matrix NXM
	
};

