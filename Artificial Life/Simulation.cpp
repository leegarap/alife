#include "Simulation.h"
const char *ground_path = "assets/dirt.png";
const char *tree_path = "assets/grass.png";
const char *water_path = "assets/water.png";
const char *carn_path = "assets/carn.png";
const char *orm_path = "assets/orm.png";
const char * herb_path = "assets/herb.png";
const int delay_frame = 1000 / FPS;
struct save_detail {
	int canvas[50][50];
};
Simulation::Simulation()
{	//Init terrain with random
	srand((unsigned)time(0));
	//initRandom();

	//Init terrain with LoadMap
	initSave("Map.map");

	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow("Artificial life", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, W, H, 0);
	renderer = SDL_CreateRenderer(window, -1, 0);
	//import texture 
	//0:ground, 1:water, 2:plant       Organism 3:C, 4:H, 5:O	
	texs.push_back(LoadTexture(ground_path));
	texs.push_back(LoadTexture(water_path));
	texs.push_back(LoadTexture(tree_path));
	texs.push_back(LoadTexture(carn_path));
	texs.push_back(LoadTexture(herb_path));
	texs.push_back(LoadTexture(orm_path));

	
	
	
	
	
	isRunning = true;
	//std::cout << "Passed" << std::endl;
	
}


SDL_Texture * Simulation::LoadTexture(const char *path){
	SDL_Surface *tempS = IMG_Load(path);
	SDL_Texture *tex = SDL_CreateTextureFromSurface(renderer, tempS);
	SDL_FreeSurface(tempS);
	return tex;

}
void Simulation::Visualize(int step,int delay) {
	int cur_frame;
	int this_step = step;
	while (step-- && isRunning) {
		std::cout << "Step:" << this_step-step  << std::endl;
		cur_frame = SDL_GetTicks();
		
		Update();
		
		Render();
		
		EventHandler();
		

		cur_frame = SDL_GetTicks() - cur_frame;
		if (cur_frame < delay_frame) {
			SDL_Delay(delay_frame - cur_frame);
		}
		SDL_Delay(delay);system("cls");
	}
}
void Simulation::Render() {
	SDL_RenderClear(renderer);


	
	DrawTerrain();
	DrawOrganism();
	SDL_RenderPresent(renderer);

}
void Simulation::EventHandler() {
	SDL_Event evt;
	SDL_PollEvent(&evt);
	switch(evt.type) {
	case SDL_QUIT:
		isRunning = false;
		break;
	default:
		break;
	}
	

}

void Simulation::DrawTexture(SDL_Texture *Tex, SDL_Rect rec) {
	SDL_RenderCopy(renderer, Tex, NULL, &rec);

}
void Simulation::DrawOrganism() {
	SDL_Rect pos;
	pos.x = 0;
	pos.y = 0;
	pos.w = scaleW;
	pos.h = scaleH;
	for (auto &o : org) {
		pos.x = o->getPos().first*scaleW;
		pos.y = o->getPos().second*scaleH;
		switch (o->getType()) {
		case 'C':
			DrawTexture(texs[3], pos);
			break;
		case 'H':
			DrawTexture(texs[4], pos);
			break;
		case 'O':
			DrawTexture(texs[5], pos);
			break;
		default:

			break;
		}
	}

}
void Simulation::DrawTerrain() {
	int type;

	SDL_Rect pos;
	pos.x = 0;
	pos.y = 0;
	pos.w = scaleW;
	pos.h = scaleH;
	for (int i = 0; i < M; i++) {
		for (int j = 0; j < N; j++) {
			pos.x = j * scaleW;
			pos.y = i * scaleH;
			type = World[i][j];
			DrawTexture(texs[type], pos);
		}
	}

}

Simulation::~Simulation()
{
	for (auto &i : texs) {
		SDL_DestroyTexture(i);
	}
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();

}
void Simulation::initRandom() {
	World.initTerrain(N, M);
	int organism = MAX_ORG;
	int fHP, dHP, size, sMin=0, curSpeed, sMax, age=0, reproductive_rate=0, posX, posY, radius;
	char type;
	
	ANN *brain;
	
	while (organism >= 0) {
		
		posX = rand() % M;
		posY = rand() % N;

		if (World.canOccupy(posX, posY)) {
			

			std::shared_ptr< Organism > curOrg=nullptr, curOrg2=nullptr;
			size = (rand() % MaxSize) + 1;
			sMax= (rand() % MaxSpeed) + 1;
			radius = (rand() % MaxRadius) + 1;
			curSpeed = (rand() % sMax);
			fHP = (rand() % (size * 5)) + 1;
			dHP = (rand() % (size * 6)) + 1;
			brain = new ANN(INPUT_SIZE(radius), HIDDEN_SIZE, OUTPUT_SIZE);
			switch (rand()%3)
			{
			case 0:
				type = 'C';
				curOrg =std::make_shared<Carnivore>(*new Carnivore(fHP, dHP, size, sMin, sMax, age, reproductive_rate, posX, posY, radius, brain) );
				break;
			case 1:
				type = 'H';
				curOrg = std::make_shared<Herbivore>(*new  Herbivore(fHP, dHP, size, sMin, sMax, age, reproductive_rate, posX, posY, radius, brain) );
				break;
			case 2:
				type = 'O';
				curOrg = std::make_shared<Omnivore>(* new Omnivore(fHP, dHP, size, sMin, sMax, age, reproductive_rate, posX, posY, radius, brain) );
				break;
			default:
				type = 'O';
				curOrg = std::make_shared<Omnivore>(*new  Omnivore(fHP, dHP, size, sMin, sMax, age, reproductive_rate, posX, posY, radius, brain) );
				break;
			}
			
			curOrg->show();
			if (!World.setOrganism(&curOrg, posX, posY)) {
				printf("Can't Set Organism\n");
			}
			else {
				
				std::cout << "Set Organism!" << std::endl;
				
			}


			//std::cout << "passed!" << std::endl;

			Simulation::org.emplace_back(curOrg);
			//std::cout << curOrg.use_count() << "\n";
			curOrg.reset();
			
			std::cout << org.back()->getType() << std::endl;
			//curOrg2.reset();
			organism--;
			//std::cout << "PassLoop" << std::endl;
			
		}
		else;
	}
	//std::cout << "\n";
	system("cls");
}

void Simulation::initSave(const char *save_path) {
	FILE *f = fopen(save_path, "rb");
	
	struct save_detail temp;
	int tempArray[50][50];
	fread(&temp, sizeof(temp), 1, f);
	for (int i = 0; i < 50; i++) {
		for (int j = 0; j < 50; j++) {
			
			tempArray[i][j] = temp.canvas[i][j];
			std::cout << tempArray[i][j] << " ";
		}
		std::cout << "\n";
	}
	
	fclose(f);

	World.initTerrainBySave(N, M, tempArray);
	initRandom();


}
void Simulation::addOrganism(Organism *orga) {
	std::shared_ptr< Organism > curOrg;
	if (orga->getType() == 'C') {
		curOrg=std::make_shared<Carnivore>(orga);
	}
	else if (orga->getType() == 'O'){
		curOrg=std::make_shared<Omnivore>(orga);
	}
	else if (orga->getType() == 'H') {
		curOrg=std::make_shared<Herbivore>(orga);
	}
	else return;
	if (!World.setOrganism(&curOrg, orga->getPos().first, orga->getPos().second)) {
		printf("Can't Set Organism\n");
	}
	else {

		std::cout << "Set Organism!" << std::endl;
		Simulation::org.emplace_back(curOrg);

	}

}

void Simulation::Update() {
	//World.showTable();
	for (auto &o:org) {
		o->action(World);
		
	}//std::cout << "Pass" << std::endl;

	
	
	for (auto &o : org) {
		o->is_Dead();
		o->show();
		//std::cout << "Shared_ptr Used:"<<o.use_count() << std::endl;
		if (o->is_Dead()) {
			World.setOrganism(nullptr, o->getPos().first, o->getPos().second);
		}
	}
	//std::cout << "-----------------------\n";
	//(org.front())->show();
	//std::cout << "Shared_ptr Used:" << org.front().use_count() << std::endl;
	//org.erase(org.begin());
	//std::cout << "Pass!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << std::endl;
	org.erase(std::remove_if(org.begin(), org.end(), [](std::shared_ptr<Organism> o) {
		return o->is_Dead();
	}),org.end());
	
}