#include "ANN.h"

/*time_t t;
int random() {
	srand((unsigned)time(&t));
	return rand();
}*/

ANN::ANN(int input, int hidden, int output)
{
	inputs = input;
	hiddens = hidden;
	outputs = output;


	HIDDEN_W = new Matrix(inputs, hiddens);

	OUTPUT_W = new Matrix(hiddens, outputs);
}
Matrix ANN::feedforward(Matrix &INPUT) {
	Matrix HIDDEN_L = INPUT.CrossTo(*HIDDEN_W);
	HIDDEN_L.relu();
	Matrix OUTPUT = HIDDEN_L.CrossTo(*OUTPUT_W);
	return OUTPUT;
}
void ANN::mutation(double mutation_rate) {
	//Mutation Hidden_w
	Matrix temp_hidden= *HIDDEN_W;
	temp_hidden.reshape(1, temp_hidden.shape()[0] * temp_hidden.shape()[1]);
	int tempN= temp_hidden.shape()[0], tempM= temp_hidden.shape()[1];
	std::vector < std::vector < double > > temp_h_v= temp_hidden.getMatrixAsVector();
	for (int i = 0; i < temp_h_v[0].size(); i++) {
		if ((rand() % 10000)/10000>mutation_rate) {
			temp_h_v[0][i]= (double)(rand()%100000) / 100000;
		}
		else {

		}
	}
	delete(HIDDEN_W);
	HIDDEN_W = new Matrix(temp_h_v[0], 1, tempN*tempM);
	HIDDEN_W->reshape(tempN, tempM);

	//Mutation OUTPUT_w
	temp_hidden = *OUTPUT_W;
	temp_hidden.reshape(1, temp_hidden.shape()[0] * temp_hidden.shape()[1]);
	tempN = temp_hidden.shape()[0], tempM = temp_hidden.shape()[1];
	temp_h_v = temp_hidden.getMatrixAsVector();
	for (int i = 0; i < temp_h_v[0].size(); i++) {
		if ((rand() % 10000) / 10000<mutation_rate) {
			temp_h_v[0][i] = (double)(rand() % 100000) / 100000;
		}
		else {

		}
	}
	delete(OUTPUT_W);
	OUTPUT_W = new Matrix(temp_h_v[0], 1, tempN*tempM);
	OUTPUT_W->reshape(tempN, tempM);
}

ANN::~ANN()
{
	if(HIDDEN_W!=NULL)delete(HIDDEN_W);
	if (OUTPUT_W != NULL)delete(OUTPUT_W);
}
ANN ANN::CrossingOverTo(ANN &A,int new_input,int new_hidden) {
	//Crossing Over Hidden Layer
	int max_size= new_input* new_hidden, maxN= new_input, maxM= new_hidden;
	Matrix this_hidden = *HIDDEN_W,that_hidden=A.getHidden();
	this_hidden.reshape(1, this_hidden.shape()[0] * this_hidden.shape()[1]);
	that_hidden.reshape(1, that_hidden.shape()[0] * that_hidden.shape()[1]);
	std::vector < std::vector < double > > this_h_v = that_hidden.getMatrixAsVector(), that_h_v=this_hidden.getMatrixAsVector(),new_h;
	/*if (this_h_v[0].size() >= that_h_v[0].size()) {
		maxN = this_hidden.shape()[0];
		maxM = this_hidden.shape()[1];
		max_size = this_h_v[0].size();
	}
	else {
		maxN = that_hidden.shape()[0];
		maxM = that_hidden.shape()[1];
		max_size = that_h_v[0].size();
	}*/
	new_h.push_back(std::vector < double >(max_size));
	for (int i = 0; i < max_size; i++) {
		if (i < this_h_v[0].size() && i < that_h_v[0].size()) {
			if ((rand() % 10000) / 10000 >= 0.5) {
				new_h[0].push_back(this_h_v[0][i]);
			}
			else {
				new_h[0].push_back(that_h_v[0][i]);
			}
		}
		else {
			if (that_h_v[0].size() >= i) {
				new_h[0].push_back(that_h_v[0][i]);
			}
			else if(this_h_v[0].size() >= i) {
				new_h[0].push_back(this_h_v[0][i]);
			}
			else {
				int random = rand() % 100000;
				double out = (double)(random*1.0) / 100000;
				new_h[0].push_back(out);
			}

		}
	}
	Matrix NEW_HIDDEN = Matrix(new_h[0],maxN,maxM);

	//crossing over output layer
	//----------------------------------------------------------------
	Matrix this_output = *OUTPUT_W, that_output = A.getOutput();
	this_output.reshape(1, this_output.shape()[0] * this_output.shape()[1]);
	that_output.reshape(1, that_output.shape()[0] * that_output.shape()[1]);
	std::vector < std::vector < double > > this_o_v = that_output.getMatrixAsVector(), that_o_v = this_output.getMatrixAsVector(), new_o;
	/*if (this_o_v[0].size() >= that_o_v[0].size()) {
		maxN = this_output.shape()[0];
		maxM = this_output.shape()[1];
		max_size = this_o_v[0].size();
	}
	else {
		maxN = that_output.shape()[0];
		maxM = that_output.shape()[1];
		max_size = that_o_v[0].size();
	}*/ 
	maxN = new_hidden;
	maxN = that_output.shape()[1];
	max_size = maxN* maxN;
	new_o.push_back(std::vector < double >(max_size));
	for (int i = 0; i < max_size; i++) {
		if (i < this_o_v[0].size() && i < that_o_v[0].size()) {
			if ((rand() % 10000) / 10000 >= 0.5) {
				new_o[0].push_back(this_o_v[0][i]);
			}
			else {
				new_o[0].push_back(that_o_v[0][i]);
			}
		}
		else {
			if (that_o_v[0].size() >= i) {
				new_o[0].push_back(that_o_v[0][i]);
			}
			else if (this_o_v[0].size() >= i) {
				new_o[0].push_back(this_o_v[0][i]);
			}
			else {
				int random = rand() % 100000;
				double out = (double)(random*1.0) / 100000;
				new_o[0].push_back(out);
			}

		}
	}
	Matrix NEW_OUTPUT = Matrix(new_h[0], maxN, maxM);

	ANN new_brain(new_input, new_hidden, that_output.shape()[1]);
	new_brain.setCustomHidden(&NEW_HIDDEN);
	new_brain.setCustomOutput(&NEW_OUTPUT);
	return new_brain;
}

Matrix ANN::getHidden() {
	return *HIDDEN_W;
}
Matrix ANN::getOutput() {
	return *OUTPUT_W;
}

void ANN::setCustomHidden(Matrix *newHidden) {
	HIDDEN_W = newHidden;
	inputs = newHidden->shape()[0];
	hiddens = newHidden->shape()[1];
}
void ANN::setCustomOutput(Matrix *newOutput) {
	OUTPUT_W = newOutput;
	outputs = newOutput->shape()[1];
	hiddens = newOutput->shape()[0];
}