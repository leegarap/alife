#include "Matrix.h"
#include<ctime>
#include<cstdlib>
#include<cstdio>



Matrix::Matrix(int N,int M)
{
	
	
	std::vector< std::vector <double> > new_mat(N);
	for (int i = 0; i < N; i++) {
		new_mat[i] = std::vector<double>(M);
	}
	int old_i = 0, old_j = 0;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			int random = rand() % 100000;
			double out = (double)(random*1.0) / 100000;
			new_mat[i][j] = out;
		}
	}
	mat = new_mat;
	shape_N = N;
	shape_M = M;
}
Matrix::Matrix(std::vector<double> &raw,int N, int M)
{
	if (N*M != raw.size()) {

		std::cout << "can\'t reshape raw to that size!\n";
		return;
	}
	std::vector< std::vector <double> > new_mat(N);
	int i_raw=0;
	for (int i = 0; i < N; i++) {
		new_mat[i] = std::vector<double>(M);
	}
	int old_i = 0, old_j = 0;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			new_mat[i][j] = raw[i_raw];
			i_raw++;
		}
	}
	mat = new_mat;
	shape_N = N;
	shape_M = M;
}

Matrix::~Matrix()
{
}

std::vector < std::vector < double >> Matrix::getMatrixAsVector() {
	return mat;
}
void Matrix::reshape(int N, int M) {
	if (shape_N*shape_M != N * M) {

		std::cout << "can\'t reshape to that size!\n";
		return ;
	}
	std::vector< std::vector <double> > new_mat(N);
	for (int i = 0; i < N; i++) {
		new_mat[i] = std::vector<double>(M);
	}
	int old_i = 0, old_j = 0;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			if (old_j == shape_M) {
				old_j = 0;
				old_i++;
			}
			new_mat[i][j] = mat[old_i][old_j];
			old_j++;
		}
	}
	mat = new_mat;
	shape_N = N;
	shape_M = M;
}
int * Matrix::shape() {
	int shape[2]= {shape_N, shape_M};
	return shape;
}


void Matrix::show() {
	for (int i = 0; i < shape_N; i++) {
		std::cout << "|";
		for (int j = 0; j < shape_M; j++) {
			std::cout << mat[i][j] << "|";
		}
		std::cout << std::endl;
 }
}
Matrix Matrix::CrossTo(Matrix &A) {
	if (shape_M != A.shape()[0]) {
		std::cout << shape_M << " " << A.shape()[0] << std::endl;
		std::cout << "Can't cross the matrice\n";
		return Matrix(0,0);
	}
	std::vector < double > new_mat;
	std::vector<std::vector<double> > matA = A.getMatrixAsVector();

	for (int i = 0; i < shape_N; i++) {
		for (int j = 0; j < A.shape()[1]; j++) {
			double out = 0;
			for (int k = 0; k < shape_M; k++) {
				out += mat[i][k] * matA[k][j];
				
			}new_mat.push_back(out);
		}
	}
	//std::cout << new_mat.size() << std::endl;
	Matrix new_matrix(new_mat,shape_N, A.shape()[1]);
	return new_matrix;
}
void Matrix::relu() {
	for (int i = 0; i < shape_N; i++) {
		for (int j = 0; j < shape_M; j++) {
			mat[i][j] = mat[i][j] < 0 ? 0 : mat[i][j];
		}
	}
}