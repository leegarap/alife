#include "Terrain.h"


Terrain::Terrain()
{
	//srand((unsigned)time(0));
	
}
void Terrain::initTerrain(int N, int M) {
	Terrain::N = N;
	Terrain::M = M;
	
	for (int i = 0; i < N; i++) {
		table.emplace_back(std::vector<int>());
		occp.emplace_back( std::vector<int>() );
		org.emplace_back(std::move(std::vector<std::shared_ptr<Organism> >()));
		res.emplace_back(std::vector< resourceObject* >());
		for (int j = 0; j < M; j++) {
			//std::cout << rand() % 3 << std::endl;
			occp[i].push_back(false );
			int randID = rand() % 3;
			table[i].push_back(randID);
			switch (randID) {
			case 0:
				res[i].emplace_back(new resourceObject("G",j,i,0,0,0) );
				break;
			case 1:
				res[i].emplace_back(new resourceObject("W",j,i, WATER, 0, GR));
				break;
			case 2:
				res[i].emplace_back(new resourceObject("P",j,i ,0, FOOD, GR));
				break;
			}
			org[i].emplace_back(nullptr);
			
		}
	}
}
void Terrain::initTerrainBySave(int N, int M, int map[50][50]) {
	Terrain::N = N;
	Terrain::M = M;

	for (int i = 0; i < N; i++) {
		table.emplace_back(std::vector<int>());
		occp.emplace_back(std::vector<int>());
		org.emplace_back(std::move(std::vector<std::shared_ptr<Organism> >()));
		res.emplace_back(std::vector< resourceObject* >());
		for (int j = 0; j < M; j++) {
			//std::cout << rand() % 3 << std::endl;
			occp[i].push_back(false);
			table[i].push_back(map[i][j]);
			switch (map[i][j]) {
			case 0:
				res[i].emplace_back(new resourceObject("G", j, i, 0, 0, 0));
				break;
			case 1:
				res[i].emplace_back(new resourceObject("W", j, i, WATER, 0, GR));
				break;
			case 2:
				res[i].emplace_back(new resourceObject("P", j, i, 0, FOOD, GR));
				break;
			}
			org[i].emplace_back(nullptr);

		}
	}



}
void Terrain::showTable() {
	std::cout << " Print Table!!:\n";
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			if (occp[i][j]) {
				std::cout << (char)((org[i][j]->getType())-'A'+'a') << " ";
			}
			else std::cout << res[i][j]->getResourceName() << " ";
		}
		std::cout << std::endl;
	}
}

std::vector<std::shared_ptr<Organism> > Terrain::findOrganism(Organism &O) {
	int curX = O.getPos().first, curY = O.getPos().second, radius = O.getRadius();
	int minX = std::max( 0, curX - radius ), minY = std::max(0, curY - radius);
	int maxX = std::min(M-1, curX + radius), maxY = std::min(N-1, curY + radius);
	std::vector< std::shared_ptr<Organism> > output;
	for (int i = minY; i <= maxY; i++) {
		for (int j = minX; j <= maxX; j++) {
			if (j == curX && i == curY)continue;

			if (org[i][j] == nullptr)continue;
			if(org[i][j]->is_Dead())continue;
			output.emplace_back(org[i][j]);

		}
	}
	return output;

}

std::vector< std::pair<int, int> > Terrain::findEmptySpace(Organism &O) {
	int curX = O.getPos().first, curY = O.getPos().second, radius = O.getRadius();
	int minX = std::max(0, curX - radius), minY = std::max(0, curY - radius);
	int maxX = std::min(M - 1, curX + radius), maxY = std::min( N - 1, curY + radius);
	std::vector<std::pair<int,int> > output;
	for (int i = minY; i <= maxY; i++) {
		for (int j = minX; j <= maxX; j++) {
			if (j == curX && i == curY)continue;

			if (!occp[i][j] && table[i][j]==0) {
				output.emplace_back(std::make_pair(i,j));
			}

		}
	}
	return output;
}

std::vector<resourceObject *> Terrain::findWater(Organism &O) {
	int curX = O.getPos().first, curY = O.getPos().second, radius = O.getRadius();
	int minX = std::max(0, curX - radius), minY = std::max(0, curY - radius);
	int maxX = std::min(M - 1, curX + radius), maxY = std::min(N - 1, curY + radius);
	std::vector<resourceObject *> output;
	for (int i = minY; i <= maxY; i++) {
		for (int j = minX; j <= maxX; j++) {
			if (j == curX && i == curY)continue;

			if (table[i][j] == 1) {
				output.push_back(res[i][j]);
			}
			

		}
	}
	return output;

}

std::vector<resourceObject *> Terrain::findPlant(Organism &O) {
	int curX = O.getPos().first, curY = O.getPos().second, radius = O.getRadius();
	int minX = std::max(0, curX - radius), minY = std::max(0, curY - radius);
	int maxX = std::min(M - 1, curX + radius), maxY = std::min(N - 1, curY + radius);
	std::vector<resourceObject *> output;
	for (int i = minY; i <= maxY; i++) {
		for (int j = minX; j <= maxX; j++) {
			if (j == curX && i == curY)continue;

			if (table[i][j] == 2) {
				output.push_back(res[i][j]);
			}


		}
	}
	return output;

}
std::vector<double> Terrain::getEnv(Organism &O) {
	int curX = O.getPos().first, curY = O.getPos().second, radius = O.getRadius();
	int minX = curX - radius, minY=curY - radius;
	int maxX = curX + radius, maxY = curY + radius;
	//std::cout << "Radius:" << radius << "From" << " (" << curX << "," << curY << ")"<<" ("<<minX<<","<<minY<<")"<<"---->"<< "(" << maxX << "," << maxY << ")" << std::endl;
	std::vector<double> output;
	for (int i = minY; i <= maxY; i++) {
		for (int j = minX; j <= maxX; j++) {
			if (j == curX && i == curY)continue;
			
			if (i < 0 || j < 0 || i>N-1 || j>M-1) {
				output.push_back(0.0);
			}
			else if (occp[i][j]) {
				output.push_back(0.1);
			}
			else if (table[i][j]==0) {
				output.push_back(1.0);
			}
			else if (table[i][j] == 1) {
				output.push_back(0.2);
			}
			else if (table[i][j] == 2) {
				output.push_back(0.5);
			}
		}
	}
	return output;

}
Terrain::~Terrain()
{
}

bool Terrain::canOccupy(int x, int y) {
	if (x < 0 || y < 0 || x >= M || y >= N) { 
		//printf("OverPlane!!\n");
		return false; 
	}
	if (table[y][x] == 0 && occp[y][x] == 0) { return true; }
	else { 
		//printf("Occ:%d.\n", occp[y][x]);
		return false; 
	}
}

bool Terrain::changePosition(int from_x, int from_y, int to_x, int to_y) {
	if (canOccupy(to_x, to_y)) {
		occp[to_y][to_x] = true;
		occp[from_y][from_x] = false;
		org[to_y][to_x] = std::move(org[from_y][from_x]);
		org[from_y][from_x] = nullptr;
		return true;
	}
	else {

		return false;
	}
}

bool Terrain::setOrganism(std::shared_ptr< Organism > *org, int x, int y) {
	
	if (org == nullptr) {
		
		occp[y][x] = false;
		Terrain::org[y][x].reset();
		return true;
	}
	if (canOccupy(x, y)) {
		occp[y][x] = true;
		Terrain::org[y][x] = *org;
		return true;
	}
	else {

		return false;
	}
}
