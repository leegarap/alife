#include "Herbivore.h"



Herbivore::Herbivore(int fHP, int dHP, int size, int sMin, int sMax, int age, int reproductive_rate, int posX, int posY, int radius, ANN *brain) :Organism('H', fHP, dHP, size, sMin, sMax, age, reproductive_rate, posX, posY, radius, brain)
{
}


Herbivore::~Herbivore()
{
}

void Herbivore::eat(Terrain &t) {
	Organism::eatPlant(t);
}

int Herbivore::think(Terrain &T) {

	double want_water = (double)getdHP() / (double)getMaxdHP(), want_plant = (double)getfHP() / (double)getMaxfHP();
	//std::cout << want_water << " " << want_org << std::endl;
	int x = posX, y = posY;
	std::vector<std::shared_ptr<Organism> > org = T.findOrganism(*this);
	std::vector<resourceObject * > water_res = T.findWater(*this);
	std::vector<resourceObject * > plant_res = T.findPlant(*this);

	if (want_water <= 0.5 && want_plant <= 0.5 && (!plant_res.empty() && !water_res.empty())) {
		return want_water > want_plant ? 5 : 4;
	}
	else if (want_water <= 0.5 && !water_res.empty()) {
		return 5;
	}
	else if (want_plant <= 0.5 && !plant_res.empty()) {
		return 4;
	}



	double to_move_x = 0.0, to_move_y = 0.0;
	if (want_water >= 0.5 && want_plant >= 0.5)for (auto &o : org) {
		if (o->getType() == 'H') {
			to_move_x += 0.5*((double)(o->getPos().first) - x);
			to_move_y += 0.5* ((double)(o->getPos().second) - y);
		}
		else {
			to_move_x -= 1.1 * ((double)(o->getPos().first) - x);
			to_move_y -= 1.1 * ((double)(o->getPos().second) - y);
		}
	}
	if (want_water < want_plant)for (auto w : water_res) {
		to_move_x +=( 1-want_water) * ((double)(w->getPos().first) - x);
		to_move_y += (1-want_water )* ((double)(w->getPos().second) - y);
	}
	if (want_water >= want_plant)for (auto p : plant_res) {
		to_move_x += (1-want_plant) * ((double)(p->getPos().first) - x);
		to_move_y += (1-want_plant) * ((double)(p->getPos().second) - y);
	}

	if (std::abs(to_move_x) == 0 && std::abs(to_move_y) == 0) {
		return rand() % 4;
	}
	else {
		if (std::abs(to_move_x) > std::abs(to_move_y)) {
			if (to_move_x > 0)return 1;
			else return 3;
		}
		else {
			if (to_move_y > 0)return 0;
			else return 2;
		}

	}


}
