#pragma once
#include "iostream"
#include "organism.h"
#include "Omnivore.h"
#include "Carnivore.h"
#include "Herbivore.h"
#include "Terrain.h"
#include <cstdio>
#include <cstdlib>
#include<ctime>
#include< Windows.h>//for Sleep()
#include <SDL.h>
#include <SDL_image.h>
#define MAX_ORG 50
#define M 50
#define N 50
#define MaxSize 50
#define MaxSpeed 5
#define MaxRadius 10
#define HIDDEN_SIZE 10
#define OUTPUT_SIZE 7
#define INPUT_SIZE(radius) (9+(2*radius+1)*(2*radius+1)-1)
#define W 500
#define H 500
#define scaleW W/M
#define scaleH H/N
#define FPS 5



class Simulation
{
public:
	Simulation();

	~Simulation();
	
	void Visualize(int step, int delay);
	
	
	

private:
	Terrain World;
	std::vector< std::shared_ptr< Organism > > org;
	SDL_Window *window;
	SDL_Renderer *renderer;
	std::vector<SDL_Texture *> texs;//0:ground, 1:water, 2:plant       Organism 3:C, 4:H, 5:O
	bool isRunning;
	void initRandom();
	void initSave(const char *save_path);
	void DrawTexture(SDL_Texture *Tex, SDL_Rect rec);
	SDL_Texture * LoadTexture(const char *path);
	void Render();
	void Update();
	void DrawTerrain();
	void DrawOrganism();
	void EventHandler();
	void addOrganism(Organism *orga );
};

