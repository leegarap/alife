#pragma once
#include<algorithm>

#include <vector>
#include "resourceObject.h"
#include<cstdlib>
#include<ctime>
#include "ANN.h"

class Terrain;

class Organism
{
public:
	Organism(char type, int fHP, int dHP, int size, int sMin, int sMax, int age, int reproductive_rate, int posX, int posY, int radius,ANN *brain);//Construct Organism
	~Organism();
	std::pair<int, int> getPos();//get Position of Organism in 2d plane

	//Return some feature of the organism
	int getRadius() const;
	int getSpeed() const;
	int getSize() const;
	int getfHP() const;
	int getdHP() const;
	int getMaxfHP() const;
	int getMaxdHP() const;
	char getType() const;

	//Action
	//change statuse of the organism
	void increaseSize(int c) ;
	void increaseSpeed(int c)  ;
	void increasefHP(int c)  ;
	void increasedHP(int c) ;
	bool is_Dead() ;



	//Action Organism
	
	void drink(Terrain &t);//def drink is the action to increase dHP of the organism.
	void move(int direction, Terrain &t);//def move is the action to move in 4 direct.
	void reproduction(Terrain &t);//def reproduction is the action to reproduction and Evolution.
	virtual void eat(Terrain &t);
	virtual int think(Terrain &T);
	//def think is the action using Artifical Neural Network to Compute an action from Orgranism state.
	void action(Terrain &t);
	
	//Show status
	void show();
protected:
	/*
	integer:
		type: type of Organism, O ->Omnivorm , H->Herbivorm, C->Carnivorm
		fHP: amount of Food in the Organism
		maxfHP: size*5
		dHP: amount of Water in the organism
		maxdHP: size*6
		size: Size of Organism
		curSpeed: current Speed of Organism
		sMax: maximun speed of the organism
		age : Time units that remain to live of the creature
		reproductive_rate : I forgot it
		posX: Position of Organism in X-Axis
		posY: Position of Organism in Y-Axis
		radius: radius relating to area that the organism can do action
	Artificial Neural Network:
		brain: address of Neural Netwoerk of the creature
	bool:
		isAlive: Alive status of the organsim 
	
	
	
	
	
	*/
	char type;
	//1<size<20
	//maxHP=size*5
	int fHP, dHP, size, sMin,curSpeed,sMax, age, reproductive_rate,posX,posY,radius;
	ANN *brain;
	bool isAlive;
	time_t t;

	

	void eatOrganism(Terrain &t);//def eatOrganism is the action of organism to increase it fHP for Carnivorm and Omnivorm.
	void eatPlant(Terrain &t);//def eatPlant is the action of organism to increase it fHP for Herbivorm and Omnivorm.
	
	
};
#include "Terrain.h"

