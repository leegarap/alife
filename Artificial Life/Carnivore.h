#pragma once
#include "Organism.h"
class Carnivore :
	public Organism
{
public:
	Carnivore(int fHP, int dHP, int size, int sMin, int sMax, int age, int reproductive_rate, int posX, int posY, int radius, ANN *brain);
	void eat(Terrain &t) override;
	int think(Terrain &T) override;
	~Carnivore();
};

